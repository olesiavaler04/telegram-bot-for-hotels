"""
The module contains a location search function.

Functions
---------
search_location(str) -> list
"""

import os
import json
import re

from loguru import logger

from handlers.rapidapi_requests.base_request import LOCALE, CURRENCY, HEADERS_RAPID, request_to_api


logger.add(os.path.join('logs', 'from_api.log'), format="{time} - {level} - {message}", level='ERROR')


def search_location(city: str, clear_destination=None) -> list:
    """
    The function accesses the api and returns a list of found cities

    Args:
        city: string with name of the city.
        clear_destination: location name found.
    """
    url = "https://hotels4.p.rapidapi.com/locations/v2/search"
    querystring = {"query": city, "locale": LOCALE, "currency": CURRENCY}
    response = request_to_api(url, HEADERS_RAPID, querystring)
    if response is None:
        logger.error('No answer for location search')
    else:
        pattern = r'(?<="CITY_GROUP",).+?[\]]'
        find = re.search(pattern, response)
        if find:
            data_location = json.loads(f"{{{find[0]}}}")
            cities = list()
            for dest_id in data_location['entities']:
                if city.isalpha():
                    clear_destination = re.sub(fr'<span[\s\S]+?>{city}</span>', city, dest_id['caption'])
                elif city.find(' ' or '-'):
                    clear_destination = re.sub(r'-?<span[\s\S]+?>*</span>-?,?', '', dest_id['caption'])
                cities.append({'city_name': clear_destination, 'destination_id': dest_id['destinationId']})
            return cities
        else:
            logger.error('CITY_GROUP value not found')
