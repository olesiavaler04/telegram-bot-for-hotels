"""
The module contains a basic api request function

Functions
---------
request_to_api(str, dict, dict) -> str
"""

import os
import requests

from loguru import logger

from config_data.config import RAPID_API_KEY


logger.add(os.path.join('logs', 'from_api.log'), format="{time} - {level} - {message}", level='ERROR')


HEADERS_RAPID = {
    'x-rapidapi-host': "hotels4.p.rapidapi.com",
    'x-rapidapi-key': RAPID_API_KEY
}
LOCALE = 'ru_RU'
CURRENCY = 'RUB'


def request_to_api(url: str, headers: dict, querystring: dict) -> str:
    """
    It is a template for a basic api request.

    Args:
        url: string of access to the desired query characteristic.
        headers: dictionary of access keys for working with api.
        querystring: dictionary of request parameters.
    Returns:
        The string with text of the response from the api.
    """
    try:
        response = requests.request('GET', url=url, headers=headers, params=querystring, timeout=20)
        if response.status_code == requests.codes.ok:
            return response.text
        else:
            logger.error('Api request failed: code is not ok')
    except requests.exceptions.ConnectTimeout:
        logger.error('Api request failed: timeout')
