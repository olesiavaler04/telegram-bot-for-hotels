"""
A package for working with functions that interact with rapidapi.com.

Files
-----
base_request : basic api request
get_hotels : request for a list of hotels
search_location : location search request
search_photo : request for hotel photos
"""


from . import base_request
from . import search_location
from . import get_hotels
from . import search_photo
