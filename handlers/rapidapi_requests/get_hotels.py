"""
The module contains functions for getting a list of hotels.

Functions
---------
get_response(dict) -> list
get_hotels_from_api(dict) -> list
get_hotels_bestdeal(dict, dict) -> list
"""

import os
import json
import re

from loguru import logger

from handlers.rapidapi_requests.base_request import LOCALE, CURRENCY, request_to_api, HEADERS_RAPID


logger.add(os.path.join('logs', 'from_api.log'), format="{time} - {level} - {message}", level='ERROR')


def get_response(querystring: dict) -> list:
    """
    Executes a request to the api.

    Args:
        querystring: dictionary of request parameters.
    Returns:
        A list with dictionaries of unique hotel parameters.
    """
    url = "https://hotels4.p.rapidapi.com/properties/list"
    response = request_to_api(url, HEADERS_RAPID, querystring)

    if response is None:
        logger.error('No answer for hotels search')
    else:
        pattern = r'(?<=results\":).+\}\}]'
        find = re.search(pattern, response)
        if find:
            res = json.loads(response)
            hotels = res['data']['body']['searchResults']['results']
            return hotels
        else:
            logger.error('Hotels list not found')


def get_hotels_from_api(bot_data: dict, sort_order=None) -> list:
    """
    Defines the parameters of the request and executes it.

    Args:
        bot_data: dictionary of values of the current query.
        sort_order: string with the key to determine the way hotels are sorted.
    Returns:
        A list with dictionaries of unique hotel parameters.
    """
    querystring = {"destinationId": bot_data['city_id'],
                   "pageNumber": "1",
                   "pageSize": bot_data['hotels_quantity'],
                   "checkIn": bot_data['check_in'],
                   "checkOut": bot_data['check_out'],
                   "adults1": bot_data['adult_in_room'][0],
                   "sortOrder": sort_order,
                   "locale": LOCALE,
                   "currency": CURRENCY
                   }

    if int(bot_data['rooms']) > 1:
        for room, adult in enumerate(bot_data['adult_in_room'], start=2):
            querystring[f'adults{room}'] = adult

    if bot_data['children'] is True:
        for child, age in enumerate(bot_data['children_age'], start=1):
            querystring[f'children{child}'] = age

    if bot_data['user_command'] == 'bestdeal':
        querystring['sortOrder'] = 'DISTANCE_FROM_LANDMARK'
        querystring['priceMin'] = bot_data['price_min']
        querystring['priceMax'] = bot_data['price_max']
        querystring['pageSize'] = '25'
        hotels = get_hotels_bestdeal(bot_data, querystring)
        return hotels
    else:
        if bot_data['user_command'] == 'lowprice':
            querystring['sortOrder'] = 'PRICE'
        elif bot_data['user_command'] == 'highprice':
            querystring['sortOrder'] = 'PRICE_HIGHEST_FIRST'
        hotels = get_response(querystring)
        return hotels


def get_hotels_bestdeal(bot_data: dict, query: dict) -> list:
    """
    The function is executed only when the bestdeal command is called.
    Generates a list of hotels that meets the request.
    Args:
        bot_data: dictionary of values of the current query.
        query: dictionary of request parameters.
    Returns:
        A list with dictionaries of unique hotel parameters.
    """
    found_hotels = list()

    for page in range(1, 4):
        query["pageNumber"] = str(page)
        hotels = get_response(query)
        if hotels:
            counter = 0
            for hotel in hotels:
                try:
                    if hotel['landmarks'][0].get('distance'):
                        distance_from_api = hotel['landmarks'][0]['distance'].split()[0].replace(',', '.')
                        if counter == int(bot_data['hotels_quantity']):
                            return found_hotels
                        elif float(bot_data['center_min']) <= float(distance_from_api) <= float(bot_data['center_max']):
                            found_hotels.append(hotel)
                            counter += 1
                except LookupError:
                    logger.error('Do not successful search landmarks when bestdeal')
            if counter == bot_data['hotels_quantity']:
                return found_hotels
