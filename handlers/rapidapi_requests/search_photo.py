"""
The module contains a photo search function.

Functions
---------
search_photo(int, int) -> list
"""

import os
import json
import re
import requests

from loguru import logger

from handlers.rapidapi_requests.base_request import HEADERS_RAPID


logger.add(os.path.join('logs', 'from_api.log'), format="{time} - {level} - {message}", level='ERROR')


def search_photo(hotel_id: int, photo_num: int):
    """
    The function accesses the api and returns a list of found photos.

    Args:
        hotel_id: integer value of the hotel ID.
        photo_num: integer of photos to show.
    """
    url = "https://hotels4.p.rapidapi.com/properties/get-hotel-photos"
    querystring = {"id": hotel_id}
    response = requests.request("GET", url, headers=HEADERS_RAPID, params=querystring)
    if response is None:
        logger.error('No answer for photo search')
    else:
        pattern = r'(?<=hotelImages\":).+?\}\}]'
        find = re.search(pattern, response.text)
        if find:
            res = json.loads(response.text)
            photos = list()
            if len(res['hotelImages']) > 0:
                for photo_base in range(photo_num):
                    try:
                        if res['hotelImages'][photo_base].get('baseUrl'):
                            photo_url = res['hotelImages'][photo_base]['baseUrl']
                            edit_photo_url = photo_url.replace('{size}', 'z')
                            photos.append(edit_photo_url)
                        else:
                            photos.append('Не удалось загрузить фото')
                    except LookupError:
                        logger.error('LookupError when searching for a photo')
                return photos
        else:
            logger.error('Photo list not found')
