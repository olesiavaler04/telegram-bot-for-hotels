"""
Package of functions responsible for the bot polling logic.

Packages
-----
custom_handlers : specific bot commands
default_handlers: universal bot commands
rapidapi_requests : functions of interaction with rapidapi.com
"""

from . import custom_handlers
from . import default_handlers
from . import rapidapi_requests



