"""
The module include auxiliary functions for data output.

Functions
---------
guests_count(dict) -> int
get_summary_text(dict) -> str
date_count(str, str) -> int
get_hotel_site(dict, int) -> str
get_result_text(dict, dict, int) -> str
"""

import os
import datetime

from loguru import logger

from .history import append_db_hotel


logger.add(os.path.join('logs', 'checks.log'), format="{time} - {level} - {message}", level='ERROR')


def guests_count(bot_data: dict) -> int:
    """
    Counts and return the total number of guests.

    Args:
        bot_data: dictionary of values of the current query.
    """
    guests = 0
    for man in bot_data['adult_in_room']:
        guests += int(man)

    if bot_data['children']:
        for child in bot_data['child_in_room']:
            if isinstance(child, list):
                for i in child:
                    guests += int(i)
            else:
                guests += int(child)

    return guests


def get_summary_text(bot_data: dict, text=None) -> str:
    """
    Generates and return a generalized text of the user's request.

    Args:
        bot_data: dictionary of values of the current query.
        text: string of preliminary information.
    """
    if bot_data['user_command'] in ['lowprice', 'highprice']:
        text = "Данные запроса:\nГород: {city}\nДата заезда: {check_in}\nДата выезда: {check_out}\n" \
               "Общее количество гостей {guests_quantity}\nКоличество номеров {rooms}\n" \
               "Вывести {hotels_quantity} отеля(ей)\n" \
               "Показать фотографии - {photo}".format(city=bot_data['city'],
                                                      check_in=bot_data['check_in'].strftime('%d.%m.%Y'),
                                                      check_out=bot_data['check_out'].strftime('%d.%m.%Y'),
                                                      guests_quantity=guests_count(bot_data),
                                                      rooms=bot_data['rooms'],
                                                      hotels_quantity=bot_data['hotels_quantity'],
                                                      photo=bot_data['photo_availability']
                                                      )
    elif bot_data['user_command'] == 'bestdeal':
        text = "Данные запроса:\nГород: {city}\nДата заезда: {check_in}\nДата выезда: {check_out}\n" \
               "Стоимость от {price_min} до {price_max} рублей за ночь\n" \
               "Расстояние до центра города: от {center_min} до {center_max} км\n" \
               "Общее количество гостей {guests_quantity}\nКоличество номеров {rooms}\n" \
               "Вывести {hotels_quantity} отеля(ей)\n" \
               "Показать фотографии - {photo}".format(city=bot_data['city'],
                                                      check_in=bot_data['check_in'].strftime('%d.%m.%Y'),
                                                      check_out=bot_data['check_out'].strftime('%d.%m.%Y'),
                                                      price_min=bot_data['price_min'],
                                                      price_max=bot_data['price_max'],
                                                      center_min=bot_data['center_min'],
                                                      center_max=bot_data['center_max'],
                                                      guests_quantity=guests_count(bot_data),
                                                      rooms=bot_data['rooms'],
                                                      hotels_quantity=bot_data['hotels_quantity'],
                                                      photo=bot_data['photo_availability']
                                                      )
    if bot_data['photo_availability'] == 'Да':
        text += f'\nПоказать {bot_data["photo_quantity"]} фотографий'

    return text


def date_count(check_in: str, check_out: str) -> int:
    """
    Counts and return the number of days of stay at the hotel.

    Args:
        check_in: check-in date at the hotel.
        check_out: check-out date at the hotel.
    """
    check_in = check_in.split('-')
    check_out = check_out.split('-')
    date_in = datetime.date(int(check_in[0]), int(check_in[1]), int(check_in[2]))
    date_out = datetime.date(int(check_out[0]), int(check_out[1]), int(check_out[2]))
    result = str(date_out - date_in).split()[0]
    return int(result)


def get_hotel_site(bot_data: dict, hotel_id: int) -> str:
    """
    Generates and returns the hotel's website.

    Args:
        bot_data: dictionary of values of the current query.
        hotel_id: integer of hotel ID received from the api.
    """
    pattern_site = 'https://www.hotels.com/ho{hotel_id}/?q-check-in={check_in}&q-check-out={check_out}' \
                   '&q-rooms={room_count}'.format(hotel_id=hotel_id,
                                                  check_in=str(bot_data['check_in']),
                                                  check_out=str(bot_data['check_out']),
                                                  room_count=bot_data['rooms'],
                                                  )
    for room, adult in enumerate(bot_data['adult_in_room']):
        pattern_site += f'&q-room-{room}-adults={adult}'

    if bot_data['children'] is True:
        for room, child in enumerate(bot_data['child_in_room']):
            pattern_site += f'&q-room-{room}-children={child}'

        for room, child_age in enumerate(bot_data['children_age']):
            if child_age.isdigit() is True and int(child_age) != 0:
                pattern_site += f'&q-room-{room}-child-0-age={child_age}'
            elif child_age.isdigit() is False:
                for i_child, i_age in enumerate(child_age.split(', ')):
                    pattern_site += f'&q-room-{room}-child-{i_child}-age={i_age}'
    else:
        for room in range(int(bot_data['rooms'])):
            pattern_site += f'&q-room-{room}-children=0'

    if bot_data['user_command'] == 'lowprice':
        pattern_site += '&sort-order=PRICE'
    elif bot_data['user_command'] == 'highprice':
        pattern_site += '&sort-order=PRICE_HIGHEST_FIRST'
    elif bot_data['user_command'] == 'bestdeal':
        pattern_site += '&sort-order=DISTANCE_FROM_LANDMARK'

    return pattern_site


def get_result_text(hotel: dict, bot_data: dict, user_id: int) -> str:
    """
    Generates and returns the final text of the information about the hotel at the request of the user.

    Args:
        hotel: dictionary with hotel data received from api.
        bot_data: dictionary of values of the current query.
        user_id: integer of user ID in the chat.
    """
    try:
        address = hotel['address']['streetAddress']
    except KeyError:
        address = 'уточните у менеджера'
        logger.error('get address is failed')

    try:
        dist = hotel['landmarks'][0]['distance']
    except LookupError:
        dist = 'уточните у менеджера'
        logger.error('get distance is failed')

    night_count = date_count(str(bot_data['check_in']), str(bot_data['check_out']))

    try:
        night_price = round(hotel['ratePlan']['price']['exactCurrent'])
        total_price = night_price * night_count
    except KeyError:
        night_price = 'уточните у менеджера'
        total_price = '---'
        logger.error('get price is failed')

    site = get_hotel_site(bot_data, hotel['id'])
    text = 'Отель {name}\nАдрес: {address}\nРасстояние до центра города: {dist}\n' \
           'Цена за ночь: {price}\nЦена за {night_count} ночи(ей) составит {total_price}' \
           '\nСтраница отеля: {site}'.format(name=hotel['name'],
                                             address=address,
                                             dist=dist,
                                             price=f'{night_price} руб',
                                             night_count=night_count,
                                             total_price=f'{total_price} руб',
                                             site=site,
                                             )
    append_db_hotel(hotel['name'], site, bot_data['city'], user_id)
    return text
