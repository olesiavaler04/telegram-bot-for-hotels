"""
In module are the specific functions for the 'bestdeal' command.

All functions accept as a parameter a message from the chat entered by the user.

Functions
---------
get_min_price(telebot.Message) -> None
get_max_price(telebot.Message) -> None
get_centre_distance_min(telebot.Message) -> None
get_centre_distance_max(telebot.Message) -> None
"""

from telebot.types import Message

from loader import bot
from states.user_information import UserInfoState
from keyboards.reply.for_survey import guests_button


@bot.message_handler(state=UserInfoState.price_min)
def get_min_price(message: Message) -> None:
    """The function requests the maximum cost per night that the user is willing to pay."""
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.isdigit():
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['price_min'] = message.text
        bot.send_message(message.from_user.id, 'Укажите максимальную стоимость номера за ночь')
        bot.set_state(message.from_user.id, UserInfoState.price_max, message.chat.id)
    else:
        bot.send_message(message.from_user.id, 'Стоимость должна быть введена с помощью цифр')


@bot.message_handler(state=UserInfoState.price_max)
def get_max_price(message: Message) -> None:
    """The function asks the user to specify the minimum distance to the city center."""
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    else:
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            if message.text.isdigit() and int(data['price_min']) < int(message.text):
                data['price_max'] = message.text
                bot.send_message(message.from_user.id, 'Укажите минимальное расстояние до центра города в километрах. '
                                                       'Например: 0.7 или 1')
                bot.set_state(message.from_user.id, UserInfoState.center_min, message.chat.id)
            else:
                bot.send_message(message.from_user.id, 'Максимальная стоимость должна быть выше минимальной '
                                                       'и введена с помощью цифр')


@bot.message_handler(state=UserInfoState.center_min)
def get_centre_distance_min(message: Message) -> None:
    """The function asks the user to specify the maximum distance to the city center."""
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.find('.' or ',') or message.text.isdigit():
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['center_min'] = message.text
            if message.text.find(','):
                true_value = message.text.replace(',', '.')
                data['center_min'] = true_value

        bot.send_message(message.from_user.id, 'Укажите максимальное расстояние до центра города в километрах. '
                                               'Например: 2.9 или 5')
        bot.set_state(message.from_user.id, UserInfoState.center_max, message.chat.id)
    else:
        bot.send_message(message.from_user.id, 'Расстояние должно быть указано с помощью цифр\n'
                                               'Если число дробное, его необходимо разделять точкой')


@bot.message_handler(state=UserInfoState.center_max)
def get_centre_distance_max(message: Message) -> None:
    """The function clarifies: only adults or with children will be settled."""
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.find('.' or ',') or message.text.isdigit():
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['center_max'] = message.text
            if message.text.find(','):
                true_value = message.text.replace(',', '.')
                data['center_max'] = true_value
            if float(data['center_min']) < float(data['center_max']):
                bot.send_message(message.from_user.id, 'Кто будет гостем отеля?', reply_markup=guests_button())
                bot.set_state(message.from_user.id, UserInfoState.guests_type, message.chat.id)
            else:
                bot.send_message(message.from_user.id,
                                 'Максимальное расстояние до центра должно быть больше минимального расстояния')
    else:
        bot.send_message(message.from_user.id, 'Расстояние должно быть указано с помощью цифр\n'
                                               'Если число дробное, его необходимо разделять точкой')
