"""
In module are the main functions that process the survey steps.

All functions accept as a parameter a message from the chat entered by the user.

Functions
---------
user_query(telebot.Message) -> None
get_city(telebot.Message) -> None
get_guests(telebot.Message) -> None
get_rooms(telebot.Message) -> None
get_room_adults(telebot.Message) -> None
get_room_child(telebot.Message) -> None
children_age(telebot.Message) -> None
get_hotels(telebot.Message) -> None
get_photo(telebot.Message) -> None
get_photo_quantity(telebot.Message) -> None
"""

from telebot.types import Message

from loader import bot
from .history import append_db_user
from states.user_information import UserInfoState
from states.service import WorkingState
from keyboards.inline.cities import city_markup
from keyboards.reply.for_survey import assent_button, top_hotels_and_photos, get_result_button
from .additional import get_summary_text


@bot.message_handler(commands=['lowprice', 'highprice', 'bestdeal'])
def user_query(message: Message) -> None:
    """Requests the user for the city in which to search for hotels."""
    append_db_user(message.from_user.id, message.text[1:])
    bot.set_state(message.from_user.id, UserInfoState.city, message.chat.id)
    with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
        data['user_command'] = message.text[1:]
    text = 'Сейчас мы начнём подбирать список {command}. \nЕсли на каком-то этапе вы решите прервать поиск, введите ' \
           'слово: выход\nПосле этого вы сможете обратиться к любой команде бота.'
    if data['user_command'] == 'lowprice':
        text = text.format(command='самых дешёвых отелей по вашему запросу')
    if data['user_command'] == 'highprice':
        text = text.format(command='самых дорогих отелей по вашему запросу')
    if data['user_command'] == 'bestdeal':
        text = text.format(command='отелей, подходящих по цене и расположению от центра города')
    bot.send_message(message.chat.id, text)
    bot.send_message(message.chat.id, 'Введите город, в котором планируется поиск отелей')


@bot.message_handler(state=UserInfoState.city)
def get_city(message: Message) -> None:
    """
    Offers options for city locations.
    Location of the next step: keyboards.inline.cities.
    """
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text[:2].isalpha():
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['city'] = message.text
        city_keyboard = city_markup(message.text.capitalize(), data)
        if city_keyboard:
            bot.send_message(message.from_user.id, 'Уточните локацию', reply_markup=city_keyboard)
        else:
            bot.send_message(message.from_user.id, 'Город не найден. Проверьте корректность ввода и повторите запрос')
    else:
        bot.send_message(message.from_user.id, 'Название города должно состоять из букв')


@bot.message_handler(state=UserInfoState.guests_type)
def get_guests(message: Message) -> None:
    """Requests how many rooms are needed to move in."""
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text not in ['Только взрослые', 'Взрослые с детьми']:
        bot.send_message(message.from_user.id, 'Необходимо выбрать кнопку с подходящим вариантом')
    else:
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['adult_in_room'] = list()
            if message.text == 'Только взрослые':
                data['children'] = False
            elif message.text == 'Взрослые с детьми':
                data['children'] = True
                data['child_in_room'] = list()
                data['children_age'] = list()
            bot.send_message(message.from_user.id, 'Сколько номеров необходимо подобрать для заселения гостей?')
            bot.set_state(message.from_user.id, UserInfoState.rooms, message.chat.id)


@bot.message_handler(state=UserInfoState.rooms)
def get_rooms(message: Message) -> None:
    """Asks the user how many adults need to be accommodated in one of the rooms."""
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.isdigit():
        if 0 < int(message.text) <= 8:
            with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
                data['rooms'] = message.text
            bot.send_message(message.from_user.id, 'Сколько взрослых необходимо заселить в первый номер?')
            bot.set_state(message.from_user.id, UserInfoState.adults, message.chat.id)
        else:
            bot.send_message(message.from_user.id, 'Минимальное количество номеров - 1\n'
                                                   'Максимальное количество номеров - 8')
    else:
        bot.send_message(message.from_user.id, 'Количество комнат должно быть указано с помощью цифр')


@bot.message_handler(state=UserInfoState.adults)
def get_room_adults(message: Message) -> None:
    """
    The function works if the number of rooms is more than two.
    Requests the number of adults or children in the room.
    Called as many times as necessary to fill all rooms.
    """
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.isdigit():
        if 0 < int(message.text) <= 20:
            with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
                data['adult_in_room'].append(message.text)

            if data['children'] is True and int(data['rooms']) >= len(data['adult_in_room']):
                bot.send_message(message.from_user.id, 'Сколько детей необходимо заселить в номер {num}?\n'
                                                       'Если в этом номере не будет детей - '
                                                       'укажите 0.'.format(num=len(data['adult_in_room'])))
                bot.set_state(message.from_user.id, UserInfoState.child_in_room, message.chat.id)
            elif int(data['rooms']) > len(data['adult_in_room']):
                bot.send_message(message.from_user.id, 'Сколько гостей необходимо заселить в номер {num}'.format(
                    num=len(data['adult_in_room']) + 1))
                bot.set_state(message.from_user.id, UserInfoState.adults, message.chat.id)
            elif int(data['rooms']) == len(data['adult_in_room']):
                bot.send_message(message.from_user.id, 'Сколько отелей необходимо вывести?',
                                 reply_markup=top_hotels_and_photos())
                bot.set_state(message.from_user.id, UserInfoState.hotels_quantity, message.chat.id)
        else:
            bot.send_message(message.from_user.id, 'Минимальное количество взрослых гостей - 1\n'
                                                   'Максимальное количество взрослых гостей - 20')
    else:
        bot.send_message(message.from_user.id, 'Количество гостей должно быть указано с помощью цифр')


@bot.message_handler(state=UserInfoState.child_in_room)
def get_room_child(message: Message) -> None:
    """
    The function works if there are children among the guests.
    Requests age of children if there are children.
    Requests the number of hotels if there are no children in the room.
    It can be called as many times as necessary to fill all the rooms.
    """
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.isdigit():
        if 0 <= int(message.text) <= 6:
            with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
                data['child_in_room'].append(message.text)

            bot.set_state(message.from_user.id, UserInfoState.children_age, message.chat.id)
            if message.text == '1':
                bot.send_message(message.from_user.id, 'Укажите возраст ребёнка')
            elif int(message.text) > 1:
                bot.send_message(message.from_user.id, f'Укажите возраст каждого ребенка через запятую и пробел.'
                                                       f'Например: 5, 13')
            elif message.text == '0' and int(data['rooms']) > len(data['adult_in_room']):
                data['children_age'].append('0')
                bot.send_message(message.from_user.id, 'Сколько взрослых необходимо заселить в номер {num}'.format(
                    num=len(data['adult_in_room']) + 1))
                bot.set_state(message.from_user.id, UserInfoState.adults, message.chat.id)
            else:
                bot.send_message(message.from_user.id, 'Сколько отелей необходимо вывести?',
                                 reply_markup=top_hotels_and_photos())
                bot.set_state(message.from_user.id, UserInfoState.hotels_quantity, message.chat.id)
        else:
            bot.send_message(message.from_user.id, 'Максимальное количество детей в номере - 6\n'
                                                   'Если в номер не нужно заселять детей, укажите цифру 0')
    else:
        bot.send_message(message.from_user.id, 'Количество детей должно быть указано с помощью цифр')


@bot.message_handler(state=UserInfoState.children_age)
def children_age(message: Message) -> None:
    """
    Function works when it is necessary to move children into a room.
    It can be called as many times as necessary to fill all the rooms.
    Requests the number of adults, if there are empty rooms left.
    Requests the number of hotels if there are no available rooms left.
    """
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    else:
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            last_data = data['child_in_room'][-1]
            if message.text.isalpha():
                bot.send_message(message.from_user.id, 'Возраст должен быть указан с помощью цифр')
            elif int(last_data) != len(message.text.split(', ')):
                bot.send_message(message.from_user.id, 'Повторите ввод.\nЧисла должны быть указаны через запятую '
                                                       'и пробел; количество чисел не должно превышать '
                                                       'количество указанных детей ')
            elif int(last_data) >= 1 and len(list(filter(lambda x: int(x) == 0 or int(x) >= 18,
                                                         message.text.split(', ')))) >= 1:
                bot.send_message(message.from_user.id, 'Проверьте корректность указанных данных и повторите ввод.\n'
                                                       'Возраст ребёнка должен быть в диапазоне от 1 до 17')
            elif int(data['rooms']) == len(data['adult_in_room']):
                data['children_age'].append(message.text)
                bot.send_message(message.from_user.id, 'Сколько отелей необходимо вывести?',
                                 reply_markup=top_hotels_and_photos())
                bot.set_state(message.from_user.id, UserInfoState.hotels_quantity, message.chat.id)
            else:
                data['children_age'].append(message.text)
                bot.send_message(message.from_user.id, 'Сколько взрослых необходимо заселить в номер {num}'.format(
                    num=len(data['adult_in_room']) + 1
                ))
                bot.set_state(message.from_user.id, UserInfoState.adults, message.chat.id)


@bot.message_handler(state=UserInfoState.hotels_quantity)
def get_hotels(message: Message) -> None:
    """Requests the need to show photos of hotels."""
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.isdigit():
        bot.send_message(message.chat.id, 'Показать фотографии отелей?', reply_markup=assent_button())
        bot.set_state(message.from_user.id, UserInfoState.photo_availability, message.chat.id)

        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['hotels_quantity'] = message.text
    else:
        bot.send_message(message.from_user.id, 'Количество отелей должно быть введено с помощью цифр')


@bot.message_handler(state=UserInfoState.photo_availability)
def get_photo(message: Message) -> None:
    """
    The function works if the user wants to see a photo.
    If the user needs a photo, requests how many photos are  to show.
    If the user does not need a photo, the request text is displayed
        and location of the next step: .checks.
    """
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    else:
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['photo_availability'] = message.text
        if message.text == 'Да':
            bot.send_message(message.from_user.id, 'Сколько фотографий отеля показать?',
                             reply_markup=top_hotels_and_photos())
            bot.set_state(message.from_user.id, UserInfoState.photo_quantity, message.chat.id)
        elif message.text == 'Нет':
            text = get_summary_text(data)
            bot.send_message(message.from_user.id, text, reply_markup=get_result_button())
            bot.set_state(message.from_user.id, WorkingState.print_result, message.chat.id)
        else:
            bot.send_message(message.from_user.id, 'Выберите один из предложенных вариантов')


@bot.message_handler(state=UserInfoState.photo_quantity)
def get_photo_quantity(message: Message) -> None:
    """
    The function works if the user wants to see a photo.
    Shows the request text.
    Location of the next step: .checks.
    """
    if message.text.lower() == 'выход':
        bot.send_message(message.from_user.id, 'Процесс поиска отелей прерван по вашей инициативе')
        bot.delete_state(message.from_user.id, message.chat.id)
    elif message.text.isdigit():
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['photo_quantity'] = message.text
        text = get_summary_text(data)
        bot.send_message(message.from_user.id, text, reply_markup=get_result_button())
        bot.set_state(message.from_user.id, WorkingState.print_result, message.chat.id)
    else:
        bot.send_message(message.from_user.id, 'Количество фотографий должно быть введено с помощью цифр')
