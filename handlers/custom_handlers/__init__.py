"""
The package contains a scenario for completing the questionnaire.

Files
-----
additional.py : auxiliary functions for data output
basic_query_step.py : the main functions of the questionnaire
bestdeal.py : specific functions for the 'bestdeal' command
checks.py : functions that make checks to output the result
history.py : specific functions for the 'history' command
"""

from . import basic_query_step
from . import additional
from . import bestdeal
from . import checks
