"""
In module are the specific functions for the 'history' command.

Functions
---------
get_search_history(telebot.Message) -> None
append_db_user(int, str) -> None
append_db_hotel(str, str, str, int) -> None
check_last_entry(int) -> None
"""

import peewee
from loguru import logger
from telebot.types import Message

from database.models import *
from loader import bot

logger.add(os.path.join('logs', 'history.log'), format="{time} - {level} - {message}", level='ERROR')


@bot.message_handler(commands='history')
def get_search_history(message: Message) -> None:
    """
    The function displays the user's query history.

    Args:
        message: command 'history'.
    """
    check_last_entry(message.from_user.id)
    try:
        if UserCommand.select().count() > 0:
            with db:
                cmd_result = UserCommand.select().where(UserCommand.user_chat_id == message.from_user.id)
                bot.send_message(message.chat.id, 'История вашего поиска')
                for cmd in cmd_result:
                    date = cmd.date_query.strftime('%H:%M %d.%m.%Y')
                    city = HotelsName.get(HotelsName.command_id == cmd.user_cmd_id).city
                    text = f'Команда: {cmd.command}\nВремя запроса: {date}\n' \
                           f'Город: {city}\nСписок отелей:'
                    entry_about_hotels = HotelsName.select().where(HotelsName.command_id == cmd.user_cmd_id)
                    for entry in entry_about_hotels:
                        text += f'\n{entry.hotel_name}'
                    bot.send_message(message.chat.id, text, parse_mode='HTML', disable_web_page_preview=True)
        else:
            bot.send_message(message.chat.id, 'История вашего поиска пока пуста')
    except peewee.OperationalError:
        logger.error('Error when get history')


def append_db_user(user_id: int, user_cmd: str) -> None:
    """
    Here an entry is added to the table of users in the database.

    Args:
        user_id: integer value of the user ID.
        user_cmd: string of name of the command that was used.
    """
    check_last_entry(user_id)
    try:
        with db:
            UserCommand.create(user_chat_id=user_id, command=user_cmd)
    except peewee.OperationalError:
        logger.error('Error when create UserCommand')


def append_db_hotel(hotel_name: str, link: str, city: str, user_id: int) -> None:
    """
    Here an entry is added to the hotels table in the database.

    Args:
        hotel_name: string of name of the hotel.
        link: string of hotel website.
        city: string of name of the city where the hotel is located.
        user_id: integer value of the user ID.
    """
    try:
        with db:
            cmd_id = UserCommand.select(
                UserCommand.user_cmd_id).where(UserCommand.user_chat_id == user_id
                                               ).order_by(UserCommand.user_cmd_id.desc()).limit(1).get()
            name = f'<a href="{link}">{hotel_name}</a>'
            HotelsName.create(command_id=cmd_id, hotel_name=name, city=city)
    except peewee.OperationalError:
        logger.error('Error when create HotelsName')


def check_last_entry(user_id: int) -> None:
    """
    The function checks for requests that were unsuccessful.
    If the request did not lead to the formation of a list of hotels,
        the unsuccessful entry is deleted from the table with users.

    Args:
        user_id: integer value of the user ID.
    """
    try:
        if UserCommand.select().where(UserCommand.user_chat_id == user_id).count() > 0:
            cmd_id = UserCommand.select(
                UserCommand.user_cmd_id).where(UserCommand.user_chat_id == user_id
                                               ).order_by(UserCommand.user_cmd_id.desc()).limit(1).get()
            last_entry = UserCommand.get(UserCommand.user_cmd_id == cmd_id)
            if HotelsName.select().where(HotelsName.command_id == cmd_id).count() == 0:
                UserCommand.delete_instance(last_entry)
    except peewee.OperationalError:
        logger.error('Error when check last entry')
