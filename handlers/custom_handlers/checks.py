"""
In module are the functions that perform checks to output the result.

All functions accept as a parameter a message from the chat entered by the user.

Functions
---------
check_for_keyboard(telebot.Message) -> None
print_results(telebot.Message) -> None
"""
import os

from loguru import logger
from telebot.apihelper import ApiException
from telebot.types import Message

from loader import bot
from states.service import WorkingState
from handlers.custom_handlers.additional import get_result_text
from handlers.rapidapi_requests.get_hotels import get_hotels_from_api
from handlers.rapidapi_requests.search_photo import search_photo


logger.add(os.path.join('logs', 'checks.log'), format="{time} - {level} - {message}", level='ERROR')


@bot.message_handler(state=WorkingState.dates)
def check_for_keyboard(message: Message):
    """
    The function works when the user enters data from the keyboard when selecting dates.
    Indicates to the user that it is necessary to use special buttons.
    """
    bot.send_message(message.from_user.id, 'Для продолжения необходимо воспользоваться предложенными вариантами')


@bot.message_handler(state=WorkingState.print_result)
def print_results(message: Message):
    """The function displays information about hotels to the user."""
    if message.text == 'Получить результат':
        bot.send_message(message.from_user.id, 'Ищем отели...')

        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            hotels = get_hotels_from_api(data)

        if hotels is None:
            bot.send_message(message.from_user.id,
                             'Не удалось выполнить ваш запрос. Попробуйте изменить критерии поиска.')
            bot.delete_state(message.from_user.id, message.chat.id)
            logger.error('Hotels list is None')
        else:
            for hotel in hotels:
                text = get_result_text(hotel, data, message.from_user.id)
                bot.send_message(message.from_user.id, text, disable_web_page_preview=True)
                if data['photo_availability'] == 'Да':
                    photos = search_photo(hotel['id'], int(data['photo_quantity']))
                    if photos:
                        for photo in photos:
                            try:
                                bot.send_photo(message.from_user.id, photo)
                            except ApiException:
                                bot.send_message(message.from_user.id, 'Не удалось загрузить фото')
                                logger.error('Photos list is None')

            bot.send_message(message.from_user.id, 'Поиск завершён.\nВам представлены все результаты, '
                                                   'которые мы нашли по заданным критериям.')

            bot.delete_state(message.from_user.id, message.chat.id)
    else:
        bot.send_message(message.from_user.id, 'Нажмите на кнопку для продолжения')
