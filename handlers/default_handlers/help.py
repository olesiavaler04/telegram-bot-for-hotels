"""
The module contains one function that processes the help command.

Functions
---------
bot_help(telebot.Message) -> None
"""


from telebot.types import Message

from config_data.config import COMMANDS
from loader import bot


@bot.message_handler(commands=['help'])
def bot_help(message: Message) -> None:
    text = [f'/{command} - {desk}' for command, desk in COMMANDS]
    bot.send_message(message.from_user.id, '\n'.join(text))
