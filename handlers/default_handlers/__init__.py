"""
Package of universal bot commands.

Files
-----
help : processes the help command
start : processes the start command
unwanted_text : responds to messages outside the script

"""


from . import start
from . import help
from . import unwanted_text
