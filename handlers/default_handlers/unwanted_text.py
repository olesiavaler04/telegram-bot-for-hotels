"""
The module contains one function that responds to messages outside the script.

Functions
---------
bot_unwanted_text(telebot.Message) -> None
"""


from telebot.types import Message

from loader import bot


@bot.message_handler(content_types=['text'])
def bot_unwanted_text(message: Message) -> None:
    """Responds to messages outside the script."""
    bot.reply_to(message, "Необходимо использовать специальные команды, чтобы общаться с ботом. Введите /help")
