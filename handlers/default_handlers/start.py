"""
The module contains one function that processes the start command.

Functions
---------
bot_start(telebot.Message) -> None
"""


from telebot.types import Message

from loader import bot


@bot.message_handler(commands=['start'])
def bot_start(message: Message) -> None:
    bot.send_message(message.from_user.id, f"Привет, {message.from_user.full_name}!")
