"""
This module loads environment variables and defines constants for the bot to work.
"""

import os

from dotenv import load_dotenv, find_dotenv

if not find_dotenv():
    exit('Переменные окружения не загружены т.к отсутствует файл .env')
else:
    load_dotenv()

BOT_TOKEN = os.getenv('BOT_TOKEN')
RAPID_API_KEY = os.getenv('RAPID_API_KEY')
COMMANDS = (
    ('start', "Запустить бота"),
    ('help', "Вывести справку"),
    ('lowprice', "Вывести самые дешёвые отели в городе"),
    ('highprice', "Вывести самые дорогие отели в городе"),
    ('bestdeal', "Вывести отели, подходящие по цене и расположению от центра"),
    ('history', "Вывести историю поиска отелей")
)
