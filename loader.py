"""
The module creates the necessary instances for the bot to work.
"""

from telebot import TeleBot
from telebot.storage import StateMemoryStorage

from config_data import config
from database.models import UserCommand, HotelsName

storage = StateMemoryStorage()
bot = TeleBot(token=config.BOT_TOKEN, state_storage=storage)  # Bot object in telegram.

"""Tables for the database."""
UserCommand.create_table()
HotelsName.create_table()
