"""
The module contains a state class for functions without changing the state.

Classes
-------
WorkingState
"""

from telebot.handler_backends import State, StatesGroup


class WorkingState(StatesGroup):
    """Generates states for performing tasks outside the polling sequence."""
    print_result = State()
    dates = State()
