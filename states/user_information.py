"""
The module contains a state class for functions with state changes.

Classes
-------
UserInfoState
"""

from telebot.handler_backends import State, StatesGroup


class UserInfoState(StatesGroup):
    """Generates states for completing tasks in a polling sequence."""
    city = State()
    rooms = State()
    guests_type = State()
    adults = State()
    child_in_room = State()
    children_age = State()
    hotels_quantity = State()
    photo_availability = State()
    photo_quantity = State()
    price_min = State()
    price_max = State()
    center_min = State()
    center_max = State()
