"""
A package for registering user states during the survey.

Files
-----
service : for functions without changing states
user_information : for functions with changing states
"""

from . import user_information
from . import service
