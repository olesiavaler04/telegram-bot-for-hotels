"""
This is the main module that runs the program.
The elements of this module work on the library: telegram_bot_calendar.
"""

import os

from telebot.custom_filters import StateFilter

from loader import bot
import handlers
from utils.set_bot_commands import set_commands


if __name__ == '__main__':
    bot.add_custom_filter(StateFilter(bot))  # State filters are connected
    set_commands(bot)  # The commands for the bot are connected
    bot.infinity_polling()  # ensures continuous operation of the bot
