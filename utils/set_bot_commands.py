"""
The module contains a function for presenting possible commands to the bot.

Functions
---------
set_commands(telebot.TeleBot) -> None
"""


from telebot.types import BotCommand
from config_data.config import COMMANDS


def set_commands(bot):
    """The function passes the description of commands to the bot."""
    bot.set_my_commands(
        [BotCommand(*i) for i in COMMANDS]
    )
