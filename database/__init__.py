"""
This is a package for working with a database.

Files
-----
models.py : includes a description of the models
history_base.db : file with database tables
"""

from . import models
