"""
Description of database models.

Classes
-------
BaseModel
UserCommand
HotelsName
"""

import os
import datetime

from peewee import *


db = SqliteDatabase(os.path.join('database', 'history_base.db'))


class BaseModel(Model):
    """
    Basic class model.

    It is intended for inheritance by objects in which database tables will be defined.
    Inherits an object from the pewee library.

    Classes:
        Meta
    """

    class Meta:
        """
        Describes the operation of the class.

        Attributes:
            database: object that defines the location of the database file.
            order_by: string with value of the field by which the table is sorted.
        """

        database = db


class UserCommand(BaseModel):
    """
    Structure of the user data storage table.

    Attributes:
        user_chat_id: integer value of the user ID.
        user_cmd_id : integer value of the number the user's command.
        command : string of the name of the command that was used.
        date_query : datetime value of command call.
    """

    user_chat_id = IntegerField()
    user_cmd_id = AutoField(primary_key=True)
    command = CharField()
    date_query = DateTimeField(default=datetime.datetime.now)

    class Meta:
        order_by = 'user_chat_id'


class HotelsName(BaseModel):
    """
    Structure of the hotels data storage table.

    Attributes:
        hotel_id : integer value of the hotel ID
        command_id : integer value of the 'user_cmd_id' field in the 'UserCommand' table.
        hotel_name : string of name of the hotel.
        city: string of name of the city where the hotel is located.
    """

    hotel_id = AutoField(primary_key=True)
    command_id = ForeignKeyField(UserCommand, to_field='user_cmd_id')
    hotel_name = CharField()
    city = CharField()

    class Meta:
        order_by = 'hotel_id'
