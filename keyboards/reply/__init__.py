"""
A package with the buttons of the user's intended response.

Files
-----
for_survey : the keyboard that appears when answering questions
"""

from . import for_survey
