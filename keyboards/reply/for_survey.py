"""
The module contains the functions of buttons to answer the questionnaire question.

Functions
---------
assent_button() -> ReplyKeyboardMarkup
guests_button() -> ReplyKeyboardMarkup
top_hotels_and_photos() -> ReplyKeyboardMarkup
get_result_button() -> ReplyKeyboardMarkup
"""

from telebot.types import ReplyKeyboardMarkup, KeyboardButton


def assent_button() -> ReplyKeyboardMarkup:
    """Buttons for expressing consent."""
    button_yes = KeyboardButton('Да')
    button_no = KeyboardButton('Нет')
    assent_kb = ReplyKeyboardMarkup(True, True).row(button_yes, button_no)
    return assent_kb


def guests_button() -> ReplyKeyboardMarkup:
    """Buttons for specifying the type of guests."""
    only_adult = KeyboardButton('Только взрослые')
    with_children = KeyboardButton('Взрослые с детьми')
    guests_kb = ReplyKeyboardMarkup(True, True).row(only_adult, with_children)
    return guests_kb


def top_hotels_and_photos() -> ReplyKeyboardMarkup:
    """
    Buttons for specifying the quantity.
    Arise when choosing the number of hotels and photos.
    """
    three = KeyboardButton('3')
    five = KeyboardButton('5')
    seven = KeyboardButton('7')
    ten = KeyboardButton('10')
    hotels_kb = ReplyKeyboardMarkup(True, True).row(three, five, seven, ten)
    return hotels_kb


def get_result_button() -> ReplyKeyboardMarkup:
    """Button to get the result."""
    button_save = KeyboardButton('Получить результат')
    res_kb = ReplyKeyboardMarkup(True, True)
    res_kb.add(button_save)
    return res_kb
