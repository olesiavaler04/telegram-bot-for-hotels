"""
In this module, the functions of displaying buttons with locations.

Functions
---------
city_markup(str, dict) -> InlineKeyboardMarkup
city_btn_callback(CallbackQuery) -> None
"""

from datetime import date, timedelta

from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton, CallbackQuery

from handlers.rapidapi_requests.search_location import search_location
from keyboards.inline.bot_calendar import start_calendar, ALL_STEPS
from loader import bot
from states.service import WorkingState


def city_markup(found_city: str, bot_data: dict) -> InlineKeyboardMarkup:
    """
    The function returns to the user a keyboard with the names of locations.

    Args:
        found_city: string with name of the city entered by the user.
        bot_data: dictionary of values of the current query.
    """
    cities = search_location(found_city)
    if cities:
        destinations = InlineKeyboardMarkup()
        bot_data['locations'] = dict()
        for city in cities:
            destinations.add(InlineKeyboardButton(text=city['city_name'],
                                                  callback_data=f"{city['destination_id']}"))
            bot_data['locations'][city['destination_id']] = city['city_name']
        return destinations


@bot.callback_query_handler(func=lambda call: True)
def city_btn_callback(call: CallbackQuery):
    """
    Called when the user clicks the button.
    Saves the selected location value.
    Location of the next step: .bot_calendar.
    """
    with bot.retrieve_data(call.from_user.id, call.message.chat.id) as data:
        data['city_id'] = call.data
        data['city'] = data['locations'][call.data]
        del data['locations']
    bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id)
    bot.send_message(call.from_user.id, 'Сохранено')

    bot.set_state(call.from_user.id, WorkingState.dates, call.message.chat.id)
    bot.send_message(call.from_user.id, 'Выберите дату заезда')
    today = date.today()
    calendar, step = start_calendar(calendar_id=1,
                                    current_date=today,
                                    min_date=today,
                                    max_date=today + timedelta(days=548),
                                    locale="ru")

    bot.send_message(call.from_user.id, f"Выберите {ALL_STEPS[step]}", reply_markup=calendar)
