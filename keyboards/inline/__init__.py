"""
Package for working with inline keyboards

Files
-----
bot_calendar : keyboard for selecting dates
cities : keyboard for selecting cities
"""


from . import bot_calendar
from . import cities
