"""
Here are the functions for displaying the calendar.
The elements of this module work on the library: telegram_bot_calendar.

Classes
-------
MyStyleCalendar

Functions
---------
start_calendar(bool, **kwargs) -> tuple
calendar_process_first(CallbackQuery) -> None
calendar_process_second(CallbackQuery) -> None
"""

from datetime import date, timedelta

from telegram_bot_calendar import DetailedTelegramCalendar
from telebot.types import CallbackQuery

from loader import bot
from states.user_information import UserInfoState
from keyboards.reply.for_survey import guests_button


ALL_STEPS = {'y': 'год', 'm': 'месяц', 'd': 'день'}


class MyStyleCalendar(DetailedTelegramCalendar):
    """Object modification class for calendar view."""
    prev_button = "⬅️"
    next_button = "➡️"
    empty_month_button = ""
    empty_year_button = ""
    empty_nav_button = "--"
    size_year = 1


def start_calendar(is_process=False, callback_data=None, **kwargs) -> tuple:
    """The function processes the data for the calendar view"""
    if is_process:
        result, key, step = MyStyleCalendar(
            calendar_id=kwargs.get('calendar_id'),
            current_date=kwargs.get('current_date'),
            min_date=kwargs.get('min_date'),
            max_date=kwargs.get('max_date'),
            locale=kwargs.get('locale')
        ).process(callback_data.data)
        return result, key, step
    else:
        calendar, step = MyStyleCalendar(
            calendar_id=kwargs.get('calendar_id'),
            current_date=kwargs.get('current_date'),
            min_date=kwargs.get('min_date'),
            max_date=kwargs.get('max_date'),
            locale=kwargs.get('locale')
        ).build()
        return calendar, step


@bot.callback_query_handler(func=MyStyleCalendar.func(calendar_id=1))
def calendar_process_first(call: CallbackQuery) -> None:
    """Presents a calendar for selecting the check_in date."""
    today = date.today()
    result, key, step = start_calendar(
        calendar_id=1,
        current_date=today,
        min_date=today,
        max_date=today + timedelta(days=548),
        locale='ru',
        is_process=True,
        callback_data=call
    )
    if not result and key:
        bot.edit_message_text(f"Выберите {ALL_STEPS[step]}",
                              call.from_user.id,
                              call.message.message_id,
                              reply_markup=key)
    elif result:
        with bot.retrieve_data(call.from_user.id, call.message.chat.id) as data:
            data['check_in'] = result
        bot.edit_message_text(f"Вы выбрали дату заезда: {result}",
                              call.message.chat.id,
                              call.message.message_id)

        bot.send_message(call.from_user.id, "Выберите дату выезда")
        calendar, step = start_calendar(calendar_id=2,
                                        min_date=result + timedelta(days=1),
                                        max_date=result + timedelta(days=548),
                                        locale="ru"
                                        )
        bot.send_message(call.from_user.id,
                         f"Выберите {ALL_STEPS[step]}",
                         reply_markup=calendar)


@bot.callback_query_handler(func=MyStyleCalendar.func(calendar_id=2))
def calendar_process_second(call: CallbackQuery) -> None:
    """
    Presents a calendar for selecting the check-out date.
    Location of the next step:
        if user command is bestdeal: handlers.custom_handlers.bestdeal
        else: handlers.custom_handlers.basic_query_step
    """
    with bot.retrieve_data(call.from_user.id, call.message.chat.id) as data:
        date_check_in = data['check_in']
    result, key, step = start_calendar(
        calendar_id=2,
        current_date=date_check_in,
        min_date=date_check_in + timedelta(days=1),
        max_date=date_check_in + timedelta(days=548),
        locale='ru',
        is_process=True,
        callback_data=call
    )
    if not result and key:
        bot.edit_message_text(f"Выберите {ALL_STEPS[step]}",
                              call.from_user.id,
                              call.message.message_id,
                              reply_markup=key)
    elif result:
        with bot.retrieve_data(call.from_user.id, call.message.chat.id) as data:
            data['check_out'] = result
        bot.edit_message_text(f"Вы выбрали дату выезда: {result}",
                              call.message.chat.id,
                              call.message.message_id)
        if data['user_command'] in ['lowprice', 'highprice']:
            bot.send_message(call.from_user.id, 'Кто будет гостем отеля?', reply_markup=guests_button())
            bot.set_state(call.from_user.id, UserInfoState.guests_type, call.message.chat.id)
        elif data['user_command'] == 'bestdeal':
            bot.send_message(call.from_user.id, 'Укажите минимальную стоимость номера за ночь')
            bot.set_state(call.from_user.id, UserInfoState.price_min, call.message.chat.id)
